part of 'survey_bloc.dart';

enum ReactionType {unhappy, neutral, good, excellent }

abstract class SurveyEvent {}

abstract class SurveyPressEvent extends SurveyEvent {
  late ReactionType reaction;

  SurveyPressEvent({required this.reaction});
}

class LoadSurveyEvent extends SurveyEvent {}

class ClearSurveyEvent extends SurveyEvent {}

class UpdateEventNameEvent extends SurveyEvent {
  late String eventName;
  UpdateEventNameEvent({required this.eventName});
}

class GenerateCSVFileEvent extends SurveyEvent {}

class UnhappyPressEvent extends SurveyPressEvent {
  UnhappyPressEvent({ReactionType reaction = ReactionType.unhappy})
      : super(reaction: reaction);
}

class NeutralPressEvent extends SurveyPressEvent {
  NeutralPressEvent({ReactionType reaction = ReactionType.neutral})
      : super(reaction: reaction);
}

class GoodPressEvent extends SurveyPressEvent {
  GoodPressEvent({ReactionType reaction = ReactionType.good})
      : super(reaction: reaction);
}

class ExcellentPressEvent extends SurveyPressEvent {
  ExcellentPressEvent({ReactionType reaction = ReactionType.excellent})
      : super(reaction: reaction);
}
