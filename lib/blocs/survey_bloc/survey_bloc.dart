import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:new_customer_satisfaction/data/reaction.dart';
import 'package:new_customer_satisfaction/data/reaction_repo.dart';
import 'package:permission_handler/permission_handler.dart';

part 'survey_event.dart';
part 'survey_state.dart';

class SurveyBloc extends Bloc<SurveyEvent, SurveyState> {
  SurveyBloc() : super(SurveyState()) {
    on<LoadSurveyEvent>(_loadData);
    on<ClearSurveyEvent>(_clearData);
    on<SurveyPressEvent>(_handleEvent);
    on<UpdateEventNameEvent>(_updateEventName);
  }

  void _handleEvent(SurveyPressEvent event, Emitter emit) async {
    String buttonPressed = 'None';

    switch (event.reaction) {
      case ReactionType.unhappy:
        buttonPressed ='Poor Selected - Thank You For Your Input' ;
        state.reaction.unhappyCount++;
        break;
      case ReactionType.neutral:
        buttonPressed = 'Neutral Selected - Thank You For Your Input';
        state.reaction.neutralCount++;
        break;
      case ReactionType.good:
        buttonPressed = 'Good Selected - Thank You For Your Input';
        state.reaction.goodCount++;
        break;
      case ReactionType.excellent:
        buttonPressed = 'Excellent Selected - Thank You For Your Input';
        state.reaction.excellentCount++;
        break;
      default:
        break;
    }

    ReactionRepo repo = ReactionRepo();
    await repo.updateReaction(state.reaction);
    emit(state.copyWith(buttonPressed: buttonPressed));
  }

  void _loadData(LoadSurveyEvent event, Emitter emit) async {
    ReactionRepo repo = ReactionRepo();
    state.reaction = await repo.getReaction();
  }

  void _clearData(ClearSurveyEvent event, Emitter emit) async {
    ReactionRepo repo = ReactionRepo();
    await repo.deleteAll();
    emit(state.copyWith(reaction: Reaction(), buttonPressed: 'Deleted Graph Data'));
  }

  void _updateEventName(UpdateEventNameEvent event, Emitter emit) {
    event.eventName != state.eventName ?
    emit(state.copyWith(eventName: event.eventName, buttonPressed: 'Your event name is now ' + event.eventName)) :
    emit(state.copyWith(buttonPressed: "The Event Name is Already Named '${event.eventName}'"));
  }

  void _generateCsvFile() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();}
}
