part of 'survey_bloc.dart';

enum SurveyEventStatus { initial, error, submitted }

class SurveyState {
  SurveyEventStatus status;
  late Reaction reaction;
  String buttonPressed;
  String eventName;

  SurveyState(
      {this.status = SurveyEventStatus.initial,
      Reaction? reaction,
      String? buttonPressed,
      String? eventName})
      : reaction = reaction ?? Reaction(),
        buttonPressed = buttonPressed ?? '',
        eventName = eventName ?? '';


  SurveyState copyWith({SurveyEventStatus? status, Reaction? reaction, String? buttonPressed, String? eventName}) {
    return SurveyState(
        status: status ?? this.status, reaction: reaction ?? this.reaction, buttonPressed: buttonPressed ?? this.buttonPressed, eventName: eventName ?? this.eventName);
  }
}
