import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:new_customer_satisfaction/alertDialogWidget.dart';
import 'package:new_customer_satisfaction/animationWidget.dart';
import 'package:rive/rive.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:new_customer_satisfaction/blocs/survey_bloc/survey_bloc.dart';
import 'package:new_customer_satisfaction/data/hive_db/hive_db.dart';
import 'package:new_customer_satisfaction/data/reaction.dart';
import 'package:new_customer_satisfaction/packages/theme_package/lib/theme_package.dart';
import 'package:new_customer_satisfaction/completedScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.landscapeRight, DeviceOrientation.landscapeLeft]);
  await HiveDB.instance.init();

  runApp(BlocProvider(
    create: (context) => SurveyBloc()..add(LoadSurveyEvent()),
    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Customer Satisfaction',
      theme: MerlinTheme().defaultTheme(),
      home: HomeScreen(),
    ),
  ));
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _controller = TextEditingController();
  late String codeDialog;
  late String valueText;
  bool buttonPressed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      floatingActionButton: CustomFloatingButton(),
      appBar: AppBar(
        title: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text('Customer Satisfaction Survey',
              style: Theme.of(context).textTheme.headline5),
        ),
        centerTitle: true,
      ),
      body: BlocConsumer<SurveyBloc, SurveyState>(
        builder: (context, state) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: AutoSizeText(BlocProvider.of<SurveyBloc>(context).state.eventName, style: Theme.of(context).textTheme.headline1, maxLines: 1,)
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [

                      Flexible(
                        child: AnimationWidget(
                            asset: 'assets/angry.riv',
                            text: 'Poor',
                            onPressed: () {
                              if (!buttonPressed) {
                                setState(() {
                                  buttonPressed = true;
                                });
                                BlocProvider.of<SurveyBloc>(context)
                                    .add(UnhappyPressEvent());
                              }
                            }),
                      ),
                      Flexible(
                        child: AnimationWidget(
                            asset: 'assets/meh.riv',
                            text: 'Neutral',
                            onPressed: () {
                              if (!buttonPressed) {
                                setState(() {
                                  buttonPressed = true;
                                });
                                BlocProvider.of<SurveyBloc>(context)
                                    .add(NeutralPressEvent());
                              }
                            }),
                      ),
                      Flexible(
                        child: AnimationWidget(
                            asset: 'assets/good.riv',
                            text: 'Good',
                            onPressed: () {
                              if (!buttonPressed) {
                                setState(() {
                                  buttonPressed = true;
                                });
                                BlocProvider.of<SurveyBloc>(context)
                                    .add(GoodPressEvent());
                              }
                            }),
                      ),
                      Flexible(
                        child: AnimationWidget(
                            asset: 'assets/excellent.riv',
                            text: 'Excellent',
                            onPressed: () {
                              if (!buttonPressed) {
                                setState(() {
                                  buttonPressed = true;
                                });
                                BlocProvider.of<SurveyBloc>(context)
                                    .add(ExcellentPressEvent());
                              }
                            }),
                      ),
                    ],
                  ),
                ),



              ],
          );
        },
        listener: (context, state) {
          Timer(
              const Duration(seconds: 2),
              () => setState(() {
                    buttonPressed = false;
                  }));
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(state.buttonPressed, textAlign: TextAlign.center),
            width: MediaQuery.of(context).size.width / 2,
          ));
        },
      ),
    );
  }

  FloatingActionButton CustomFloatingButton() {
    return FloatingActionButton(
        child: Icon(Icons.library_add_check_rounded,
            color: MerlinTheme().merlinPurple),
        backgroundColor: MerlinTheme().merlinGrey,
        onPressed: () => showDialog(
            barrierColor: Colors.transparent,
            barrierDismissible: false,
            context: context,
            builder: (context) {
              return AlertDialogWidget(
                  alertDialogTitle: 'Please Enter a Passcode:',
                  controller: _controller,
                  submitEvent: () {
                    if (_controller.text == 'MERLINCSAT') {
                      Navigator.pop(context);
                      _controller.clear();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              CompletedScreen()));
                    }
                  });
            }));
  }

  Widget CustomElevatedButton(String image, VoidCallback pressed) {
    return Container(
      padding: const EdgeInsets.all(8),
      child: ElevatedButton(
        child: SizedBox(
            child: RiveAnimation.asset(
          image,
        )),
        onPressed: pressed,
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.transparent),
            shadowColor: MaterialStateProperty.all(Colors.transparent)),
      ),
    );
  }

  Widget CustomerTextButton(String text, VoidCallback pressed) {
    return Container(
      padding: const EdgeInsets.only(
        top: 8,
      ),
      child: ElevatedButton(
        child: Text(
          text,
          style: Theme.of(context).textTheme.headline5,
        ),
        onPressed: pressed,
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all(Theme.of(context).primaryColor),
            shadowColor: MaterialStateProperty.all(Colors.transparent)),
      ),
    );
  }
}
