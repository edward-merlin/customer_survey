import 'package:flutter/material.dart';
import 'package:theme_package/theme_package.dart';

class AlertDialogWidget extends StatefulWidget {
  final VoidCallback submitEvent;
  final TextEditingController controller;
  final String alertDialogTitle;
   const AlertDialogWidget({Key? key, required this.submitEvent, required this.controller, required this.alertDialogTitle}) : super(key: key);

  @override
  _AlertDialogWidgetState createState() => _AlertDialogWidgetState();
}

class _AlertDialogWidgetState extends State<AlertDialogWidget> {

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(builder: (context, setState) {
      return Center(
        child: SingleChildScrollView(
          child: AlertDialog(
            backgroundColor: MerlinTheme().merlinGrey,
            content: Wrap(
              //mainAxisSize: MainAxisSize.min,
              children: [
                Text(widget.alertDialogTitle,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
                TextField(
                    controller: widget.controller,
                    decoration: InputDecoration(),
                    textAlign: TextAlign.center),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomerTextButton(
                      'Cancel',
                      () {
                        widget.controller.clear();
                        Navigator.pop(context);
                      },
                    ),
                    CustomerTextButton('Submit', widget.submitEvent)
                  ],
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget CustomerTextButton(String text, VoidCallback pressed) {
    return Container(
      padding: const EdgeInsets.only(
        top: 8,
      ),
      child: ElevatedButton(
        child: Text(
          text,
          style: Theme
              .of(context)
              .textTheme
              .headline5,
        ),
        onPressed: pressed,
        style: ButtonStyle(
            backgroundColor:
            MaterialStateProperty.all(Theme
                .of(context)
                .primaryColor),
            shadowColor: MaterialStateProperty.all(Colors.transparent)),
      ),
    );
  }
}
