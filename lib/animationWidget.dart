import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rive/rive.dart';
import 'package:theme_package/theme_package.dart';

class AnimationWidget extends StatefulWidget {
  final String asset;
  final VoidCallback onPressed;
  final String text;
  const AnimationWidget({Key? key, required this.asset, required this.onPressed, required this.text}) : super(key: key);

  @override
  _AnimationWidgetState createState() => _AnimationWidgetState();
}

class _AnimationWidgetState extends State<AnimationWidget> {
  late Artboard _artboard;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    rootBundle.load(widget.asset).then((data) async {
      final file = RiveFile.import(data);
      final artBoard = file.mainArtboard;
      var controller = SimpleAnimation(artBoard.animations.first.name);
      artBoard.addController(controller);
      setState(() {
        _artboard = artBoard;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? const CircularProgressIndicator()
        : Column(
            children: [
              ElevatedButton(
                onPressed: widget.onPressed,
                style: ElevatedButton.styleFrom(primary: MerlinTheme().merlinPurple, elevation: 10),
                child: Rive(
                  artboard: _artboard,
                  useArtboardSize: true,
                ),
              ),
              AutoSizeText(widget.text, style: Theme.of(context).textTheme.headline3,)
            ],
          );
  }
}
