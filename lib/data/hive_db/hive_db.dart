import 'package:hive_flutter/hive_flutter.dart';
import 'package:new_customer_satisfaction/data/reaction.dart';
import 'package:path_provider/path_provider.dart';

class HiveDB {
  static final HiveDB _instance = HiveDB._();
  HiveDB._();

  static HiveDB get instance => _instance;

  Future<void> init() async {
    var dir = await getApplicationSupportDirectory();
    var path = dir.path + "Projects\\new_customer_satisfaction\\lib\\data\\hive_db";
    await Hive.initFlutter(path);
    Hive.registerAdapter(ReactionAdapter());
  }

  Future<Box<dynamic>> _openBox(String className) async {
    return await Hive.openBox(className);
  }

  Future<void> putToBox(Reaction object) async {
    var box = await _openBox('Reaction');
    box.put(0, object);
  }

  Future<Reaction> get(Reaction object) async{
    var box = await _openBox('Reaction');
    return await box.get(0, defaultValue: Reaction());
  }

  Future<void> clear() async {
    var box = await _openBox('Reaction');
    await box.clear();

  }
}