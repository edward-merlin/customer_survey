// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reaction.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ReactionAdapter extends TypeAdapter<Reaction> {
  @override
  final int typeId = 0;

  @override
  Reaction read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Reaction()
      ..unhappyCount = fields[0] as int
      ..neutralCount = fields[1] as int
      ..goodCount = fields[2] as int
      ..excellentCount = fields[3] as int;
  }

  @override
  void write(BinaryWriter writer, Reaction obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.unhappyCount)
      ..writeByte(1)
      ..write(obj.neutralCount)
      ..writeByte(2)
      ..write(obj.goodCount)
      ..writeByte(3)
      ..write(obj.excellentCount);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReactionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
