

import 'package:new_customer_satisfaction/data/hive_db/hive_db.dart';
import 'package:new_customer_satisfaction/data/reaction.dart';

class ReactionRepo{
  Future<void> updateReaction(Reaction reaction) async {
    HiveDB.instance.putToBox(reaction);
  }
  Future <Reaction> getReaction() async {
    Reaction reaction = await HiveDB.instance.get(Reaction());
    return reaction;
  }

  Future<void> deleteAll() async {
    await HiveDB.instance.clear();
  }
}