import 'package:hive/hive.dart';
import 'dart:math';

part 'reaction.g.dart';

@HiveType(typeId: 0)
class Reaction extends HiveObject {
  @HiveField(0)
  int unhappyCount = 0;
  @HiveField(1)
  int neutralCount = 0;
  @HiveField(2)
  int goodCount = 0;
  @HiveField(3)
  int excellentCount = 0;

  double largestNumber() {
    var numberList = [unhappyCount, neutralCount, goodCount, excellentCount];
    var largestList = numberList[0];
    var smallestList = numberList[0];

    for (var i = 0; i < numberList.length; i++) {
      if (numberList[i] > largestList) {
        largestList = numberList[i];
      }
      if (numberList[i] < smallestList) {
        smallestList = numberList[i];
      }
    }
    return largestList.toDouble();
  }

}
