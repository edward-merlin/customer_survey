import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_customer_satisfaction/alertDialogWidget.dart';
import 'package:new_customer_satisfaction/blocs/survey_bloc/survey_bloc.dart';
import 'package:new_customer_satisfaction/data/reaction.dart';
import 'package:theme_package/theme_package.dart';
import 'main.dart';

class CompletedScreen extends StatefulWidget {
  const CompletedScreen({Key? key}) : super(key: key);

  final List<Color> availableColors = const [
    Colors.purpleAccent,
    Colors.yellow,
    Colors.lightBlue,
    Colors.orange,
    Colors.pink,
    Colors.redAccent,
  ];

  @override
  _CompletedScreenState createState() => _CompletedScreenState();
}

class _CompletedScreenState extends State<CompletedScreen> {
  late TextEditingController controller;
  late final SurveyBloc _bloc;
  late final Color barBackgroundColor;
  late final Reaction _largestNumber;
  bool buttonPressed = false;

  @override
  void initState() {
    _bloc = BlocProvider.of<SurveyBloc>(context);
    barBackgroundColor = MerlinTheme().merlinPurple;
    controller = TextEditingController();
    super.initState();
  }

  final Duration animDuration = const Duration(milliseconds: 200);

  int touchedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text('Customer Satisfaction',
              style: Theme.of(context).textTheme.headline3),
        ),
        centerTitle: true,
      ),
      body: Container(
        // margin: EdgeInsets.only(top: 80),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.only(top: 85.0),
                child: BlocBuilder<SurveyBloc, SurveyState>(
                  builder: (context, state) => BarChart(
                    mainBarData(),
                    swapAnimationDuration: animDuration,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          if (_bloc.state.reaction.largestNumber() != 0) {
                            _bloc.add(ClearSurveyEvent());
                          }
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.white)),
                        child: Text(
                          'Clear Graph Data',
                          style: Theme.of(context).textTheme.bodyText1,
                        )),

                    ElevatedButton(
                        onPressed: () =>
                            showDialog(
                                barrierColor: Colors.transparent,
                                barrierDismissible: false,
                                context: context,
                                builder: (context) {
                                  return AlertDialogWidget(
                                    alertDialogTitle: 'Please Enter Your Event Name:',
                                      controller: controller,
                                      submitEvent: () {Navigator.pop(context); _bloc.add(UpdateEventNameEvent(eventName: controller.text));
                                    });

                                }),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.white)),
                        child: Text(
                          'Add Event Name',
                          style: Theme.of(context).textTheme.bodyText1,
                        )),
                  ]),
            )
          ],
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(
    int x,
    double y, {
    bool isTouched = false,
    Color barColor = Colors.white,
    double width = 22,
    List<int> showTooltips = const [],
  }) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          y: isTouched && _bloc.state.reaction.largestNumber() != 0.0
              ? y + 1
              : y,
          colors: isTouched ? [Colors.yellow] : [barColor],
          width: width,
          borderSide: isTouched && _bloc.state.reaction.largestNumber() != 0.0
              ? const BorderSide(color: Colors.yellow, width: 1)
              : const BorderSide(color: Colors.white, width: 0),
          backDrawRodData: BackgroundBarChartRodData(
            show: _bloc.state.reaction.largestNumber() != 0.0,
            y: _bloc.state.reaction.largestNumber(),
            colors: [barBackgroundColor],
          ),
        ),
      ],
    );
  }

  List<BarChartGroupData> showingGroups() => List.generate(4, (i) {
        switch (i) {
          case 0:
            return makeGroupData(
                0, _bloc.state.reaction.unhappyCount.toDouble(),
                isTouched: i == touchedIndex);
          case 1:
            return makeGroupData(
                1, _bloc.state.reaction.neutralCount.toDouble(),
                isTouched: i == touchedIndex);
          case 2:
            return makeGroupData(2, _bloc.state.reaction.goodCount.toDouble(),
                isTouched: i == touchedIndex);
          case 3:
            return makeGroupData(
                3, _bloc.state.reaction.excellentCount.toDouble(),
                isTouched: i == touchedIndex);
          default:
            return throw Error();
        }
      });

  BarChartData mainBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        enabled: _bloc.state.reaction.largestNumber() != 0.0,
        touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: MerlinTheme().merlinGrey,
            getTooltipItem: (group, groupIndex, rod, rodIndex) {
              String weekDay;
              switch (group.x.toInt()) {
                case 0:
                  weekDay = 'Unhappy';
                  break;
                case 1:
                  weekDay = 'Neutral';
                  break;
                case 2:
                  weekDay = 'Good';
                  break;
                case 3:
                  weekDay = 'Excellent';
                  break;
                default:
                  throw Error();
              }
              return BarTooltipItem(
                weekDay + '\n',
                TextStyle(
                  color: MerlinTheme().merlinPurple,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: (rod.y - 1).toString(),
                    style: TextStyle(
                      color: MerlinTheme().merlinPurple,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              );
            }),
        touchCallback: (FlTouchEvent event, barTouchResponse) {
          setState(() {
            if (!event.isInterestedForInteractions ||
                barTouchResponse == null ||
                barTouchResponse.spot == null) {
              touchedIndex = -1;
              return;
            }
            touchedIndex = barTouchResponse.spot!.touchedBarGroupIndex;
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: SideTitles(showTitles: false),
        topTitles: SideTitles(showTitles: false),
        bottomTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) =>
              Theme.of(context).textTheme.headline3,
          margin: 5,
          getTitles: (double value) {
            switch (value.toInt()) {
              case 0:
                return 'Unhappy';
              case 1:
                return 'Neutral';
              case 2:
                return 'Good';
              case 3:
                return 'Excellent';
              default:
                return '';
            }
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
          reservedSize: 50,
          interval: 0.25,
          getTitles: (value) {
            if (value == 0) {
              return '0';
            } else if (value == _bloc.state.reaction.largestNumber() * 0.25) {
              return (_bloc.state.reaction.largestNumber() * 0.25).toString();
            } else if (value == _bloc.state.reaction.largestNumber() / 2) {
              return (_bloc.state.reaction.largestNumber() / 2).toString();
            } else if (value == _bloc.state.reaction.largestNumber()) {
              return _bloc.state.reaction.largestNumber().toString();
            } else if (value == _bloc.state.reaction.largestNumber() * 0.75) {
              return (_bloc.state.reaction.largestNumber() * 0.75).toString();
            } else {
              return '';
            }
          },
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: showingGroups(),
      gridData: FlGridData(show: false),
    );
  }
}
